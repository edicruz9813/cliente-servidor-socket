const { io } = require('../server');

// Conexion base de datos
var mysql = require('mysql');
var connection = mysql.createConnection({
    host: 'localhost',
    user: 'node_user',
    password: '123456',
    database: 'node_db'
});

connection.connect();

io.on('connection', (client) => {
    console.log('Usuario conectado');

    client.emit('enviarMensaje', {
        usuario: 'Administrador',
        mensaje: 'Bienvenido cliente'
    });

    client.on('disconnet', () => {
        console.log('Usuario desconectado');
    });

    connection.query('SELECT * from heroes', function(error, results, fields) {
        if (error) throw error;

        console.log(results);
        client.on('enviarMensaje', (mensaje, callback) => {
            console.log(mensaje);
            if (mensaje.usuario) {
                callback({
                    res: results
                });
            } else {
                callback({
                    res: 'TODO MAL'
                });
            }
            callback();
        });
    });
    // Escuchar cliente
});